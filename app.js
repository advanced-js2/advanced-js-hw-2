const books = [
    { 
      author: "Люсі Фолі",
      name: "Список запрошених",
      price: 70 
    }, 
    {
     author: "Сюзанна Кларк",
     name: "Джонатан Стрейндж і м-р Норрелл",
    }, 
    { 
      name: "Дизайн. Книга для недизайнерів.",
      price: 70
    }, 
    { 
      author: "Алан Мур",
      name: "Неономікон",
      price: 70
    }, 
    {
     author: "Террі Пратчетт",
     name: "Рухомі картинки",
     price: 40
    },
    {
     author: "Анґус Гайленд",
     name: "Коти в мистецтві",
    }
  ];

  function createBooksList (books) {
    const div = document.querySelector('#root');
    const ul = document.createElement('ul');

    div.prepend(ul);

    books.forEach((item) => {
     try{
        if(!item.name){
            throw ({
                message: 'without name',
                book: item
            });      
        }
        if(!item.author){
            throw ({
                message: 'without author',
                book: item
            });    
        }
        if(!item.price){
            throw ({
                message: 'without price',
                book: item
            });    
        }
        ul.insertAdjacentHTML('beforeend',`<li>${item.name},${item.author},${item.price}</li>`)
    }  
    catch (err) {
        console.error(err.message, err.book);
    }
    })
}


createBooksList(books);
